import requests, re, datetime
import pushover
from time import sleep


keys = {'Overnight':0,'Today':0,'Tonight':0,'Sunday':0,'Sunday Night':0,'Monday':0,'Monday Night':0,'Tuesday':0,'Tuesday Night':0,'Wednesday':0,'Wednesday Night':0,'Thursday':0,'Thursday Night':0,
'Friday':0,'Friday Night':0,'Saturday':0,'Saturday Night':0}
Day_index_locations = []
Delete_From_List = []
#Narragansett Bay Forcast Address
SEAADD = "https://forecast.weather.gov/shmrn.php?mz=anz236"
LANDADD = "https://forecast.weather.gov/MapClick.php?lat=41.699&lon=-71.4621"

R = requests

SUBTEXT = R.get(SEAADD)




WeatherS = SUBTEXT.content.index("Narragansett Bay")
WeatherE = SUBTEXT.content.index("$$")
Weather = SUBTEXT.content[WeatherS:WeatherE]
Weather = re.sub('<[^<]+?>', '', Weather).replace('&nbsp;','\b')
#print Weather

TEST = R.get(LANDADD)

A = TEST.content
B = A[A.find("Detailed Forecast"):A.find("Additional Forecasts and Information")]
C = re.sub('<[^<]+?>','',B)
for lst in keys: 
    if C.find(lst) > 0:
        keys[lst] = C.find(lst)
    else:
        Delete_From_List.append(lst)
for Obj in Delete_From_List:
    del keys[Obj]
Day_Index_Locations = sorted(keys.iteritems(), key=lambda (k,v): (v,k))

list_DICT = list(enumerate(Day_Index_Locations))
Final_Weather = ['Warwick, RI 7 Day Forecast:\n\n']
for Location, key in enumerate(list_DICT):
    if key[0] != len(list_DICT)-1:

        step_One = '%s\n %s \n\n'%(key[1][0],C[key[1][1]+len(key[1][0]):list_DICT[key[0]+1][1][1]])
        Final_Weather.append(step_One)
    else:
        Final_Weather.append(key[1][0]+'\n'+C[key[1][1]+len(key[1][0]):C.rfind('.')+1])
        

Final_Weather = ''.join(Final_Weather)
print(Weather)
print(Final_Weather)
CharacterTotal = (len(Final_Weather)+len(Weather))
print(int(round(CharacterTotal/120.0)))
#sleep(60)
Send_IT = True
while True:
    if Send_IT and datetime.datetime.now().strftime("%H") == '08':
        pushover.send_SMS(Weather)
        pushover.send_SMS(Final_Weather)
        Send_IT = False

    if datetime.datetime.now().strftime("%H") != '08':
        Send_IT = True
    sleep(1200)




from PIL import Image
import requests
from StringIO import StringIO

def White_To_Black(inputImage):
	data = inputImage.getdata()
	newCanvas = []
	for item in data:
		if item[0] == 255 and item[1] == 255 and item[2] == 255:
			newCanvas.append((0, 0, 0, 255))
		else:
				newCanvas.append(item)
	return newCanvas
def Get_Radar_Image(Location=BOX,Black_Font=True,Image_URL_List=False)
	if Image_URL_List == False:
		Image_List = ["https://radar.weather.gov/Warnings/Short/BOX_Warnings_0.gif","https://radar.weather.gov/Overlays/Cities/Short/BOX_City_Short.gif","https://radar.weather.gov/Overlays/County/Short/BOX_County_Short.gif","https://radar.weather.gov/RadarImg/N0R/BOX_N0R_0.gif","https://radar.weather.gov/Overlays/Topo/Short/BOX_Topo_Short.jpg"]
	else:
		Image_List = Image_URL_List
	Topography = Image.open(StringIO(requests.get(Image_List[-1]).content)).convert("RGBA")
	Radar = Image.open(StringIO(requests.get(Image_List[-2]).content)).convert("RGBA")
	County_lines = Image.open(StringIO(requests.get(Image_List[-3]).content)).convert("RGBA")
	Cites = Image.open(StringIO(requests.get(Image_List[-4]).content)).convert("RGBA")
	Warnings = Image.open(StringIO(requests.get(Image_List[-5]).content)).convert("RGBA")
	if Black_Font:
		Cites.putdata(White_To_Black(Cites))
		
	new_img = Image.blend(Topography,Radar, 0.5)
	new_img = Image.alpha_composite(new_img, County_lines)
	new_img = Image.alpha_composite(new_img, Cites)
	new_img = Image.alpha_composite(new_img, Warnings)
	new_img.save("%s-Radar-output.png" %(Location),"PNG")

